# Flight_Management_System
Java springboot project

FLIGHT MANAGEMENT SYSTEM

INTRODUCTION:

•	The Flight Management System is a cutting-edge, Java-based booking solution designed to simplify the process of booking and managing flight tickets. 

•	With its real-time consolidation of data from multiple airline carriers, the system provides users with accurate and up-to-date information on available flights, schedules, and rates. 

•	This intuitive and user-friendly application empowers travellers to make informed decisions about their travel plans, while also streamlining the management of bookings for administrators

PROJECT OVERVIEW :

CUSTOMER FEATURES
•	Create a new user account to access the application. 

•	Login with your username and password. 

•	Search for available flights by date and destination. 

•	Book a flight by selecting your preferred flight and providing passenger information. 

•	View past bookings, including flight details and passenger information. 

•	Cancel or modify a previously booked flight if necessary.

ADMINISTRATOR FEATURES:
•	Login into the application to manage flights. 

•	Add flight, schedule, and route details, including information such as flight numbers, departure and arrival times, and routes. 

•	View existing flight, schedule, and route details for easy reference. 

•	Cancel or modify flight, schedule, and route details as needed.

![alt text](https://github.com/a-pawar/Flight_Management_System/blob/main/images/Flight%20Management%20System%20Project%20Presentation%20(2)_page-0001.jpg)
![alt text](https://github.com/a-pawar/Flight_Management_System/blob/main/images/Flight%20Management%20System%20Project%20Presentation%20(2)_page-0002.jpg)
![alt text](https://github.com/a-pawar/Flight_Management_System/blob/main/images/Flight%20Management%20System%20Project%20Presentation%20(2)_page-0003.jpg)
![alt text](https://github.com/a-pawar/Flight_Management_System/blob/main/images/Flight%20Management%20System%20Project%20Presentation%20(2)_page-0004.jpg)
![alt text](https://github.com/a-pawar/Flight_Management_System/blob/main/images/Flight%20Management%20System%20Project%20Presentation%20(2)_page-0005.jpg)
![alt text](https://github.com/a-pawar/Flight_Management_System/blob/main/images/Flight%20Management%20System%20Project%20Presentation%20(2)_page-0006.jpg)
![alt text](https://github.com/a-pawar/Flight_Management_System/blob/main/images/Flight%20Management%20System%20Project%20Presentation%20(2)_page-0007.jpg)
![alt text](https://github.com/a-pawar/Flight_Management_System/blob/main/images/Flight%20Management%20System%20Project%20Presentation%20(2)_page-0008.jpg)
![alt text](https://github.com/a-pawar/Flight_Management_System/blob/main/images/Flight%20Management%20System%20Project%20Presentation%20(2)_page-0009.jpg)
![alt text](https://github.com/a-pawar/Flight_Management_System/blob/main/images/Flight%20Management%20System%20Project%20Presentation%20(2)_page-0010.jpg)
![alt text](https://github.com/a-pawar/Flight_Management_System/blob/main/images/Flight%20Management%20System%20Project%20Presentation%20(2)_page-0011.jpg)
![alt text](https://github.com/a-pawar/Flight_Management_System/blob/main/images/Flight%20Management%20System%20Project%20Presentation%20(2)_page-0012.jpg)
![alt text](https://github.com/a-pawar/Flight_Management_System/blob/main/images/Flight%20Management%20System%20Project%20Presentation%20(2)_page-0013.jpg)
![alt text](https://github.com/a-pawar/Flight_Management_System/blob/main/images/Flight%20Management%20System%20Project%20Presentation%20(2)_page-0014.jpg)
![alt text](https://github.com/a-pawar/Flight_Management_System/blob/main/images/Flight%20Management%20System%20Project%20Presentation%20(2)_page-0015.jpg)
